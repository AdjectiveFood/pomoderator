'use strict'
const utils = require('./utils.js')
const config = require('./config.json')
const Discord = require('discord.js')
const client = new Discord.Client()

const minute = 1000 * 60
const hour = 60 * minute
const durations = {
  pomodoro: 25 * minute,
  shortBreak: 5 * minute,
  longBreak: 10 * minute,
  breakExtension: 5 * minute,
  setupTimeout: 10 * minute,
}

const emojis = {
  getIn: '🍅',
  warning: '⚠',
  getOut: '🍕',
}
const readyMsg = [
  'Get in nerds!',
  "Let's get this bread!",
  "It's time to pomo the doro!",
  "Let's Gooooooooooooooo!",
  "Work! Work! Work! Work! Work!",
]
const getRandomReadyMessage = () => {
  return readyMsg[Math.floor(Math.random() * Math.floor(readyMsg.length))] + `\nreact with ${emojis.getIn} to join then use the go command to start`
}
const warningMessage = (messageText) => {
  return `${emojis.warning}${messageText}${emojis.warning}`
}
const self = {
  currentId: null,
  activationString: () => `<@!${self.currentId}>`,
}

/*type Pomodoro = {
    id: string;
    activeUser: { [userId: string]: User };
    pomodoroByUsers: { [userId: string]: number };
    count: number;
}*/
const activePomodoroSets/*: Pomodoro[]*/ = {
}

const startPomodoroSet = (channel) => {
  if(!activePomodoroSets[channel.id]) {
    const newPomodoro = {
      id: utils.generateUUID(),
      channel: channel,
      activeUser: {},
      pomodoroByUsers: {},
      count: 0,
      started: false,
    }
    activePomodoroSets[channel.id] = newPomodoro
    //end the set if the users haven't started it yet.
    setTimeout(() => {
      if(newPomodoro.count === 0) {
        const tag = 
        channel.send('The pomodoro got cancel, you have to use the go command')
        endPomodoroSet(newPomodoro)
      }
    }, durations.setupTimeout)
    return newPomodoro.id
  } else {
    console.error('there is already an active pomodoro set in this channel.')
  }
}

const endPomodoroSet = (pomodoroSet) => {
  const message = ["End of the set, good work everybody!"]
  for(let userId in pomodoroSet.pomodoroByUsers) {
    message.push(`${pomodoroSet.pomodoroByUsers[userId].user.username} worked a total of ${pomodoroSet.pomodoroByUsers[userId].count * 30} minutes`)
  }
  pomodoroSet.channel.send(message.join('\n'))
  delete activePomodoroSets[pomodoroSet.channel.id]
}

const addUserToPomodoroSet = (channel, user) => {
  const pomodoroSet = activePomodoroSets[channel.id]
  pomodoroSet.activeUser[user.id] = user
  if(!(user.id in pomodoroSet.pomodoroByUsers)) {
    pomodoroSet.pomodoroByUsers[user.id] = {user, count: 0}
  }
}

const removeUserFromPomodoroSet = (channel, user) => {
  delete activePomodoroSets[channel.id].activeUser[user.id]
}

const startPomodoro = (pomodoroSet) => {
  pomodoroSet.started = true
  const users = []
  pomodoroSet.count += 1
  for(let userId in pomodoroSet.activeUser) {
    users.push(userId)
    pomodoroSet.pomodoroByUsers[userId].count += 1
  }

  if(users.length === 0) {
    pomodoroSet.channel.send(warningMessage(`Nobody reacted with ${emojis.getIn}.`))
    endPomodoroSet(pomodoroSet)
    return
  }

  pomodoroSet.channel.send(users.map(id => `<@!${id}>`).join(' ') + ' Work for 25 minutes!')
  setTimeout(() => {
    endPomodoro(pomodoroSet)
  }, durations.pomodoro)
}

const endPomodoro = (pomodoroSet) => {
  pomodoroSet.started = false

  const users = Object.keys(pomodoroSet.activeUser)
  pomodoroSet.activeUser = []
  const isLongBreak = pomodoroSet.count % 4 === 0
  const message = `End of pomodoro #${pomodoroSet.count} ${users.map(id => `<@!${id}>`).join(' ')} ${isLongBreak ? '10' : '5'} minutes break!`
  pomodoroSet.channel.send(message)
  setTimeout(() => {
    pomodoroSet.channel.send(users.map(id => `<@!${id}>`).join(' ') + ` Break's over! ` + getRandomReadyMessage()).then(reply => {
      reply.react(emojis.getIn)
    })
    //end the set if the users haven't started it yet.
    setTimeout(() => {
      if(!pomodoroSet.started) {
        pomodoroSet.channel.send(warningMessage('The pomodoro has been broken'))
        endPomodoroSet(pomodoroSet)
      }
    }, durations.breakExtension)
  }, isLongBreak ? durations.longBreak : durations.shortBreak)
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`, client.user.id)
  self.currentId = client.user.id
})

client.on('message', msg => {
  const splitMsg = msg.content.toLowerCase().trim().split(/[ -]/).filter(str => str.length > 0)
  const channel = msg.channel
  if (!channel) {
    console.error('failed to find channel', channel)
    return;
  }

  if(splitMsg.length > 0 && splitMsg[0] === self.activationString()) {
    switch(splitMsg[1]) {
      case 'ready':
        if(!activePomodoroSets[channel.id]) {
          channel.send(getRandomReadyMessage()).then(reply => {
            reply.react(emojis.getIn)
            startPomodoroSet(channel)
          })
        } else {
          channel.send(warningMessage('There is already an active pomodoro in this channel'))
        }
       break
      case 'go':
       if(activePomodoroSets[channel.id]) {
          if(activePomodoroSets[channel.id].started === false) {
            startPomodoro(activePomodoroSets[channel.id])
          } else {
            channel.send(warningMessage('A pomodoro is already underway'))
          }
       } else {
          channel.send(warningMessage('There is no active pomodoro in this channel. Use the ready command first'))
        }
        break
      case 'help':
      default:
        const helpMessage = [
          'Tag me using @ followed by a space and one of the following command:',
          ' - ready (to initiate a pomodoro)',
          ' - go (to start a pomodoro with everyone who reacted with 🍅)'
        ]
        channel.send(helpMessage.join('\n'))
        break
    }
  }
})

const isValidReaction = (reaction, user) => {
  return user.id !== self.currentId
    && reaction.message.author.id === self.currentId
    && activePomodoroSets[reaction.message.channel.id]
}

client.on('messageReactionAdd', (reaction, user) => {
  if(isValidReaction(reaction, user)) {
    if(reaction.emoji.name === emojis.getIn) {
      addUserToPomodoroSet(reaction.message.channel, user)
    } else if(reaction.emoji.name === emojis.getOut) {
      removeUserFromPomodoroSet(reaction.message.channel, user)
    }
  }
})

client.on('messageReactionRemove', (reaction, user) => {
  if(isValidReaction(reaction, user)) {
    if(reaction.emoji.name === emojis.getIn) {
      removeUserFromPomodoroSet(reaction.message.channel, user)
    }
  }
})

client.login(config['DISCORD_TOKEN'])
